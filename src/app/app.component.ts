import { Component, OnInit } from '@angular/core';
import { WordpressService } from './wordpress.service';
import { isBrowser, isNode } from 'angular2-universal';
import { Angulartics2GoogleAnalytics } from 'angulartics2';
//import { MetaService } from 'ng2-meta';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  isThisBrowser = isBrowser;

  settings: any;

  constructor(private wordpress: WordpressService, angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics) {

  }

  ngOnInit() {
    this.wordpress.getSettings()
      .then(resolve => {
        this.settings = resolve.json().acf;
      })
      .catch(err => {
        console.log(err);
        return {};
      });
  }
}
