/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { TreatmentPageComponent } from './treatment-page.component';

describe('Component: TreatmentPage', () => {
  it('should create an instance', () => {
    let component = new TreatmentPageComponent();
    expect(component).toBeTruthy();
  });
});
