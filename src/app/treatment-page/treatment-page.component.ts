import { Component, OnInit, Input } from '@angular/core';

@Component({

  selector: 'treatment-page',
  templateUrl: './treatment-page.component.html',
  styleUrls: ['./treatment-page.component.scss']
})
export class TreatmentPageComponent implements OnInit {
  @Input() page: any;

  constructor() { }

  ngOnInit() {

  }

}
