/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { PriceListSectionComponent } from './price-list-section.component';

describe('Component: PriceListSection', () => {
  it('should create an instance', () => {
    let component = new PriceListSectionComponent();
    expect(component).toBeTruthy();
  });
});
