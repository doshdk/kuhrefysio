import { Component, OnInit, Input } from '@angular/core';

@Component({

  selector: 'price-list-section',
  templateUrl: './price-list-section.component.html',
  styleUrls: ['./price-list-section.component.scss']
})
export class PriceListSectionComponent implements OnInit {
  @Input() priceTables: any;
  @Input() content: any;

  constructor() { }

  ngOnInit() {
  }

}
