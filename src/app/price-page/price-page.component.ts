import { Component, OnInit, Input } from '@angular/core';

@Component({

  selector: 'price-page',
  templateUrl: './price-page.component.html',
  styleUrls: ['./price-page.component.scss']
})
export class PricePageComponent implements OnInit {
  @Input() page: any;

  constructor() { }

  ngOnInit() {
  }

}
