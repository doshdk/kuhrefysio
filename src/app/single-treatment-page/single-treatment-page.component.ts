import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WordpressService } from '../wordpress.service';

@Component({
  providers: [WordpressService],
  selector: 'single-treatment-page',
  templateUrl: './single-treatment-page.component.html',
  styleUrls: ['./single-treatment-page.component.scss']
})
export class SingleTreatmentPageComponent implements OnInit {
  private sub: any;
  private slug: any;
  private page: any = {
    acf: {

    }
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private wordpress: WordpressService
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.slug = params['slug'] ? params['slug'] : 'forsiden';

       this.wordpress.getTreatmentBySlug(this.slug)
         .then(result => {
           this.page = result;
         });

    });
  }

}
