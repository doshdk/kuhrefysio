import { Component, OnInit, Input } from '@angular/core';

@Component({

  selector: 'general-info-boxes',
  templateUrl: './general-info-boxes.component.html',
  styleUrls: ['./general-info-boxes.component.scss']
})
export class GeneralInfoBoxesComponent implements OnInit {
  @Input() settings: any;

  constructor() { }

  ngOnInit() {
  }

}
