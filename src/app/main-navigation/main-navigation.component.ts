import { Component, OnInit } from '@angular/core';
import { WordpressService } from '../wordpress.service';
import { isBrowser, isNode } from 'angular2-universal';

@Component({
  providers: [WordpressService],
  selector: 'main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.scss']
})
export class MainNavigationComponent implements OnInit {
  menu: any;
  mobileNavOpen: boolean = false;
  isThisBrowser = isBrowser;

  constructor(private wordpress: WordpressService) { }

  ngOnInit() {
    this.wordpress.getMenu('main_nav')
      .then(result => {
        this.menu = result;
      });
  }

}
