import { Component, OnInit, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'page-splash',
  templateUrl: './page-splash.component.html',
  styleUrls: ['./page-splash.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PageSplashComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;

  constructor() { }

  ngOnInit() {
  }

}
